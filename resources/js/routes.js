import MainComponent from "./components/allComponents/MainComponent.vue";
import ExampleComponent from "./components/ExampleComponent.vue";
export const  routes = [
   
    {
        path:'/',
        component:MainComponent,
        name:'MainComponent'
    },
    {
        path:'/example',
        component:ExampleComponent,
        name:'ExampleComponent'
    }

];
